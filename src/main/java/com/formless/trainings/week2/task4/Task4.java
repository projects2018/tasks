package com.formless.trainings.week2.task4;

import java.util.Arrays;

public class Task4 {

    public static void main(String[] args) {

        int[] integers = new int[]{5, 3, 2, 8, 1, 4};
        System.out.println(Arrays.toString(integers));

        for (int i = 0; i < integers.length - 1; i++) {
            for (int j = i + 1; j < integers.length; j++) {
                if (integers[i] % 2 != 0 && integers[j] % 2 != 0 && integers[i] > integers[j]) {
                    int tmp = integers[i];
                    integers[i] = integers[j];
                    integers[j] = tmp;
                }
            }
        }

        System.out.println(Arrays.toString(integers));

    }

}
