package com.formless.trainings.week2.task3;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Task3 {

    public static void main(String[] args) {

        int[] array = new int[]{2, 2, 1, 1, 1, 2, 2};
        Map<Integer, Integer> counts = new HashMap<>();

        for (int i : array) {
            if (counts.containsKey(i)) {
                counts.replace(i, counts.get(i) + 1);
            } else {
                counts.put(i, 1);
            }
        }

        Map.Entry<Integer, Integer> result = counts.entrySet().stream()
                .filter(entry -> entry.getValue() > array.length / 2)
                .findFirst().orElseThrow(() -> new RuntimeException("Wrong array!"));

        System.out.println(Arrays.toString(array));
        System.out.println(result.getKey() + " is dominant.");

    }

}
