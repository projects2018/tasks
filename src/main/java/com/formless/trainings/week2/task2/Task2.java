package com.formless.trainings.week2.task2;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Task2 {

    public static void main(String[] args) {

        List<String> words = Arrays.asList("eat", "tea", "tan", "ate", "nat", "bat");

        words.stream()
                .collect(Collectors.toMap(Function.identity(), Task2::getOrderedWord))
                .entrySet().stream()
                .collect(Collectors.groupingBy(Map.Entry::getValue))
                .values()
                .forEach(value -> System.out.println(getResult(value)));
    }

    private static String getResult(List<Map.Entry<String, String>> list) {
        return list.stream()
                .sorted(Comparator.comparing(Map.Entry::getKey))
                .map(Map.Entry::getKey)
                .collect(Collectors.joining(","));
    }

    private static String getOrderedWord(String word) {
        return word.chars()
                .map(c -> (char) c).sorted()
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }


}
