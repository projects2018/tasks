package com.formless.trainings.week2.task1;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

public class Task1 {

    public static void main(String[] args) {

        String sample = "{[](){[]}}";
        System.out.println(testString(sample));

    }

    private static boolean testString(String sample) {
        final String opening = "({[";
        final String closing = ")}]";
        final Deque<Character> stack = new ArrayDeque<>();

        final AtomicBoolean result = new AtomicBoolean(true);
        sample.chars().forEach(c -> {
            char currentChar = (char) c;
            if (opening.indexOf(c) != -1) {
                stack.addLast(currentChar);
            } else {
                char openingChar = opening.charAt(closing.indexOf(currentChar));
                if (!Objects.equals(stack.pollLast(), openingChar)) {
                    result.set(false);
                }
            }
        });

        return stack.isEmpty() && result.get();
    }

}
