package com.formless.trainings.week4.task1;

import com.formless.trainings.week3.Util;

public class Task1 {

    public static void main(String[] args) {

        BinaryTreeIterator<String> binaryTreeIterator = new BinaryTreeIterator<>(new Util().generateStringTree());

        while(binaryTreeIterator.hasNext()) {
            System.out.println(binaryTreeIterator.next());
        }

    }

}
