package com.formless.trainings.week4.task1;

import com.formless.trainings.week3.BinaryTree;
import com.formless.trainings.week3.Node;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BinaryTreeIterator<T extends Comparable> implements Iterator<T> {

    private List<Node<T>> nodesInOrder = new ArrayList<>();
    private int currentElement;

    BinaryTreeIterator(BinaryTree<T> binaryTree) {
        Node<T> currentNode = binaryTree.getRootNode();
        createPreorderList(currentNode);
    }

    @Override
    public boolean hasNext() {
        return currentElement < nodesInOrder.size();
    }

    @Override
    public T next() {
        return nodesInOrder.get(currentElement++).getValue();
    }

    private void createPreorderList(Node<T> node) {
        nodesInOrder.add(node);
        if (node.getLeftNode() != null) {
            createPreorderList(node.getLeftNode());
        }
        if (node.getRightNode() != null) {
            createPreorderList(node.getRightNode());
        }
    }

}
