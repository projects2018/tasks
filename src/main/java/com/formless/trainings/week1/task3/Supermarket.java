package com.formless.trainings.week1.task3;

import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

class Supermarket {

    void serve(List<Buyer> buyers) {

        int cashiersCount = new Random().nextInt(3) + 1;

        System.out.println("Number of cashiers: " + cashiersCount);
        System.out.println("Number of buyers: " + buyers.size());
        System.out.println("-----");

        ExecutorService executorService = Executors.newFixedThreadPool(cashiersCount);
        List<Buyer> collect = buyers.stream()
                .map(buyer -> new Buyer())
                .collect(Collectors.toList());

        List<Future<Integer>> futures = new ArrayList<>();
        try {
            futures = executorService.invokeAll(collect);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        IntSummaryStatistics collect1 = futures.stream()
                .map(future -> {
                    try {
                        return future.get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                    return 0;
                }).collect(Collectors.summarizingInt(e -> e));

        System.out.println("Seconds total: " + collect1.getSum() / 1000);

    }

}
