package com.formless.trainings.week1.task3;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.concurrent.Callable;

import static java.lang.String.format;

public class Buyer implements Callable<Integer> {

    private final int goodsAmount;

    Buyer() {
        goodsAmount = new Random().nextInt(5) + 1;
    }

    public int getGoodsAmount() {
        return goodsAmount;
    }

    @Override
    public Integer call() throws Exception {
        try {
            System.out.println(format("%s:%s: buyer {%s} came", LocalDateTime.now().getMinute(), LocalDateTime.now().getSecond(), goodsAmount));
            Thread.sleep(goodsAmount * 1000L);
            System.out.println(format("%s:%s: buyer {%s} finished", LocalDateTime.now().getMinute(), LocalDateTime.now().getSecond(), goodsAmount));
            return goodsAmount * 1000;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
