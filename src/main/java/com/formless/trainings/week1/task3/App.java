package com.formless.trainings.week1.task3;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {

    public static void main(String[] args) {

        Supermarket supermarket = new Supermarket();

        List<Buyer> buyers = Stream.generate(Buyer::new)
                .limit(new Random().nextInt(5) + 1L)
                .collect(Collectors.toList());

        supermarket.serve(buyers);
        System.exit(0);

    }

}
