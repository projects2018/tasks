package com.formless.trainings.week1.task4;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {

    public static void main(String[] args) {

        List<Integer> list1 = geterateList();
        List<Integer> list2 = geterateList();

        System.out.println(list1);
        System.out.println(list2);

        int first = 0;
        for (int el : list2) {
            boolean wasInserted = false;
            for (int i = first; i < list1.size(); i++) {
                if (list1.get(i) > el) {
                    list1.add(i, el);
                    wasInserted = true;
                    first = i;
                    break;
                }
            }
            if (!wasInserted) {
                list1.add(el);
                first = Integer.MAX_VALUE;
            }
        }

        System.out.println();
        System.out.println(list1);
        System.out.println(list2);

    }

    private static List<Integer> geterateList() {
        return Stream.generate(() -> new Random().nextInt(9) + 1)
                .limit(new Random().nextInt(5) + 1L)
                .sorted()
                .collect(Collectors.toList());
    }

}
