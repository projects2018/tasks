package com.formless.trainings.week1.task2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

class ClerkService {

    private List<Integer> cash = new ArrayList<>();

    private final int ticketPrice;

    ClerkService(int ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    boolean sell(Buyer buyer) {
        cash.add(buyer.getMoney().getNominal());
        if (buyer.getMoney().getNominal() == ticketPrice) {
            return true;
        }

        int change = buyer.getMoney().getNominal() - ticketPrice;
        List<Integer> sortedCash = cash.stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
        for (int i = 0; i < sortedCash.size(); i++) {
            Integer bond = sortedCash.get(i);
            if (bond <= change) {
                change -= bond;
                sortedCash.set(i, 0);
            }
        }
        if (change == 0) {
            cash = sortedCash;
            return true;
        }
        return false;
    }
}
