package com.formless.trainings.week1.task2;

public enum Money {
    N25(25), N50(50), N100(100);

    private int nominal;

    Money(int nominal) {
        this.nominal = nominal;
    }

    public int getNominal() {
        return nominal;
    }
}
