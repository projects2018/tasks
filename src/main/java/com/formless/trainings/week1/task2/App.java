package com.formless.trainings.week1.task2;

import java.util.Random;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {

        ClerkService clerkService = new ClerkService(25);

        boolean cantSell = Stream.generate(Buyer::new)
                .limit(new Random().nextInt(7) + 1L)
                .peek(System.out::println)
                .map(clerkService::sell)
                .anyMatch(p -> !p);

        System.out.println(!cantSell);
    }

}