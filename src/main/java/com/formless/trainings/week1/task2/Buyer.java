package com.formless.trainings.week1.task2;

import java.util.Random;

public class Buyer {
    private Money money;

    Buyer() {
        this.money = Money.values()[new Random().nextInt(3)];
    }

    Money getMoney() {
        return money;
    }

    @Override
    public String toString() {
        return "{" + money + '}';
    }
}
