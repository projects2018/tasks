package com.formless.trainings.week3.task2;

import com.formless.trainings.week3.Util;
import com.formless.trainings.week3.BinaryTree;
import com.formless.trainings.week3.Node;

public class Task2 {

    public static void main(String[] args) {
        BinaryTree<Integer> binaryTree = new Util().generateIntegerTree();
        System.out.println("Depth = " + getDepth(binaryTree));
    }

    private static int getDepth(BinaryTree binaryTree) {
        return recursiveCount(binaryTree.getRootNode());
    }

    private static int recursiveCount(Node node) {
        if (node == null)
            return 0;
        else {
            int mdl = recursiveCount(node.getLeftNode());
            int mdr = recursiveCount(node.getRightNode());
            return mdl > mdr ? mdl + 1 : mdr + 1;
        }
    }

}
