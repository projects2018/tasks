package com.formless.trainings.week3;

public class BinaryTree<T extends Comparable> {

    private Node<T> rootNode;

    public void add(T value) {
        rootNode = addRecursive(rootNode, value);
    }

    public void addRootNode(Node<T> node) {
        rootNode = node;
    }

    public Node<T> getRootNode() {
        return rootNode;
    }

    private Node<T> addRecursive(Node<T> current, T value) {
        if (current == null) {
            return new Node<>(value);
        }
        if (value.compareTo(current.getValue()) > 0) {
            current.setLeftNode(addRecursive(current.getLeftNode(), value));
        } else if (value.compareTo(current.getValue()) < 0) {
            current.setRightNode(addRecursive(current.getRightNode(), value));
        } else {
            return current;
        }

        return current;
    }
}
