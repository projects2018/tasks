package com.formless.trainings.week3.task4;

import com.formless.trainings.week3.BinaryTree;
import com.formless.trainings.week3.Node;
import com.formless.trainings.week3.Util;

import java.util.LinkedList;
import java.util.Queue;

public class Task4 {

    public static void main(String[] args) {

        BinaryTree<String> stringBinaryTree = new Util().generateStringTree();
        System.out.println(concatInCycle(stringBinaryTree));

    }

    private static String concatInCycle(BinaryTree<String> binaryTree) {
        Queue<Node> nodes = new LinkedList<>();
        nodes.add(binaryTree.getRootNode());

        StringBuilder count = new StringBuilder(binaryTree.getRootNode().getValue());
        while (!nodes.isEmpty()) {
            Node node = nodes.remove();
            if (node.getLeftNode() != null) {
                nodes.add(node.getLeftNode());
                count.append(node.getLeftNode().getValue());
            }
            if (node.getRightNode() != null) {
                nodes.add(node.getRightNode());
                count.append(node.getRightNode().getValue());
            }
        }

        return count.toString();
    }

}
