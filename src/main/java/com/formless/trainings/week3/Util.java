package com.formless.trainings.week3;

public class Util {

    public BinaryTree<Integer> generateIntegerTree() {
        BinaryTree<Integer> binaryTree = new BinaryTree<>();
        binaryTree.addRootNode(new Node<>(8));

        binaryTree.add(3);
        binaryTree.add(10);
        binaryTree.add(1);
        binaryTree.add(6);
        binaryTree.add(14);
        binaryTree.add(4);
        binaryTree.add(7);
        binaryTree.add(13);

        return binaryTree;
    }

    public BinaryTree<String> generateStringTree() {
        BinaryTree<String> binaryTree = new BinaryTree<>();
        binaryTree.addRootNode(new Node<>("q"));

        binaryTree.add("w");
        binaryTree.add("e");
        binaryTree.add("r");
        binaryTree.add("t");
        binaryTree.add("y");
        binaryTree.add("u");
        binaryTree.add("i");
        binaryTree.add("o");

        return binaryTree;
    }

}
