package com.formless.trainings.week3.task1;

import com.formless.trainings.week3.BinaryTree;
import com.formless.trainings.week3.Node;
import com.formless.trainings.week3.Util;

import java.util.LinkedList;
import java.util.Queue;

public class Task1 {

    public static void main(String[] args) {
        BinaryTree<Integer> binaryTree = new Util().generateIntegerTree();

        System.out.println("Counted recursively: " + countRecursively(binaryTree));
        System.out.println("Counted in cycle: " + countInCycle(binaryTree));

    }

    private static int countRecursively(BinaryTree binaryTree) {
        return recursiveCount(binaryTree.getRootNode());
    }

    private static int recursiveCount(Node node) {
        int count = 1;
        if (node.getLeftNode() != null) {
            count += recursiveCount(node.getLeftNode());
        }
        if (node.getRightNode() != null) {
            count += recursiveCount(node.getRightNode());
        }
        return count;
    }

    private static int countInCycle(BinaryTree binaryTree) {
        Queue<Node> nodes = new LinkedList<>();
        nodes.add(binaryTree.getRootNode());

        int count = 1;
        while (!nodes.isEmpty()) {
            Node node = nodes.remove();
            if (node.getLeftNode() != null) {
                nodes.add(node.getLeftNode());
                count += 1;
            }
            if (node.getRightNode() != null) {
                nodes.add(node.getRightNode());
                count += 1;
            }
        }

        return count;
    }

}
