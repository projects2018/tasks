package com.formless.trainings.week3.task3;

import com.formless.trainings.week3.Node;
import com.formless.trainings.week3.Util;
import com.formless.trainings.week3.BinaryTree;

public class Task3 {

    public static void main(String[] args) {
        BinaryTree<String> stringBinaryTree = new Util().generateStringTree();
        concatRecursively(stringBinaryTree);
    }

    private static void concatRecursively(BinaryTree<String> binaryTree) {
        System.out.print("inorder: ");
        recursiveConcatInorder(binaryTree.getRootNode());
        System.out.println();
        System.out.print("preorder: ");
        recursiveConcatPreorder(binaryTree.getRootNode());
        System.out.println();
        System.out.print("postorder: ");
        recursiveConcatPostorder(binaryTree.getRootNode());
    }

    private static void recursiveConcatInorder(Node<String> node) {
        String curString = node.getValue();
        if (node.getLeftNode() != null) {
            recursiveConcatInorder(node.getLeftNode());
        }
        System.out.print(curString);
        if (node.getRightNode() != null) {
            recursiveConcatInorder(node.getRightNode());
        }
    }

    private static void recursiveConcatPreorder(Node<String> node) {
        String curString = node.getValue();
        System.out.print(curString);
        if (node.getLeftNode() != null) {
            recursiveConcatPreorder(node.getLeftNode());
        }
        if (node.getRightNode() != null) {
            recursiveConcatPreorder(node.getRightNode());
        }
    }

    private static void recursiveConcatPostorder(Node<String> node) {
        String curString = node.getValue();
        if (node.getLeftNode() != null) {
            recursiveConcatPostorder(node.getLeftNode());
        }
        if (node.getRightNode() != null) {
            recursiveConcatPostorder(node.getRightNode());
        }
        System.out.print(curString);
    }

}
